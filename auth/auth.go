package auth

import (
	"context"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
	"log"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	gcontext "github.com/gorilla/context"
	"gitlab.com/bryanbarton525/pulse/metrics"
)

type ContextKey string

const ContextKeyRequestID ContextKey = "requestID"

// User is a struct to store unique id and secret
type User struct {
	UniqueID     string `json:"unique_id"`
	UniqueSecret string `json:"unique_secret"`
}

// JwtToken is a struct to hold token
type JwtToken struct {
	Token string `json:"token"`
}

// Exception is a struct to hold exception messages
type Exception struct {
	Message string `json:"message"`
}

// CreateToken creates a token for authN
func CreateToken(w http.ResponseWriter, req *http.Request) {
	metrics.TokenRequestCount.Inc()
	var user User
	ttl := 60 * time.Second
	bodyBytes, err := io.ReadAll(req.Body)
	metrics.CurrentOpenFiles.Add(1)
	if err != nil {
		log.Fatal(err)
		json.NewEncoder(w).Encode("error: unable to ready request body")
		return
	}
	err = req.Body.Close()
	if err != nil {
		log.Fatal(err)
		json.NewEncoder(w).Encode("error: an internal error has occured - Unable to close request body")
		return
	}
	metrics.CurrentOpenFiles.Sub(1)
	bodyString := string(bodyBytes)
	b, _ := b64.StdEncoding.DecodeString(bodyString)
	_ = json.Unmarshal(b, &user)

	// TODO Database call to validate uniqueID and UniqueSecret

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": user.UniqueID,
		"password": user.UniqueSecret,
		"exp":      time.Now().UTC().Add(ttl).Unix(),
	})
	tokenString, error := token.SignedString([]byte("secret")) // replace with better string
	if error != nil {
		fmt.Println(error)
	}
	metrics.TokenIssuedCount.Inc()
	json.NewEncoder(w).Encode(JwtToken{Token: tokenString})
}

// ValidateMiddleware verifies the authorization token and parses it
func ValidateMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		id := uuid.New()

		ctx = context.WithValue(ctx, ContextKeyRequestID, id.String())

		r = r.WithContext(ctx)

		log.Printf("Incomming request %s %s %s %s", r.Method, r.RequestURI, r.RemoteAddr, id.String())

		authorizationHeader := r.Header.Get("authorization")
		// verify header isnt blank
		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
			// standard authorization header looks like this: basic <token>
			// checking for length of 2
			if len(bearerToken) == 2 {
				token, error := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
					if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
						metrics.TotalAuthFailureCount.Inc()
						return nil, fmt.Errorf("there was an error")
					}
					return []byte("secret"), nil
				})
				if error != nil {
					metrics.TotalAuthFailureCount.Inc()
					w.Header().Set("Content-Type", "application/json")
					w.Header().Set("x-request-id", id.String())
					json.NewEncoder(w).Encode(Exception{Message: error.Error()})
					return
				}
				if token.Valid {
					gcontext.Set(r, "decoded", token.Claims)
					w.Header().Set("Content-Type", "application/json")
					w.Header().Set("x-request-id", id.String())
					next(w, r)
				} else {
					metrics.TotalAuthFailureCount.Inc()
					w.Header().Set("Content-Type", "application/json")
					w.Header().Set("x-request-id", id.String())
					json.NewEncoder(w).Encode(Exception{Message: "Invalid authorization token"})
				}
			}
		} else {
			metrics.TotalAuthFailureCount.Inc()
			w.Header().Set("Content-Type", "application/json")
			w.Header().Set("x-request-id", id.String())
			json.NewEncoder(w).Encode(Exception{Message: "An authorization header is required"})
		}
	})
}

// Need to add authZ authentication to validate permissions post token auth
