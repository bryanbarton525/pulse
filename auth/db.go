package auth

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/bryanbarton525/pulse/logging"
	"gitlab.com/bryanbarton525/pulse/pulseengine/pathfinder"
)

var dbLog = logging.Logger("db.log")

// CreateDB creates database
func CreateDB() {
	// search for db file and create db if it doesnt exist
	fmt.Println("Checking to see if database exists...")
	found := pathfinder.FindFile("pulse.db", "./")
	if !found {
		database, err := sql.Open("sqlite3", "./pulse.db")
		dbLog.Error(err)
		fmt.Println("DB created...")

		statement, err := database.Prepare("CREATE TABLE IF NOT EXISTS registered_apps (uid INTEGER PRIMARY KEY AUTOINCREMENT, app_name TEXT, unique_id varchar(64) NULL, unique_secret varchar(128) NULL, `created` DATE NULL)")
		dbLog.Error(err)
		res, err := statement.Exec()
		dbLog.Error(err)
		dbLog.Info(res)
		fmt.Println("table created..")
	} else {
		fmt.Println("Database already exists, skipping db creation")
	}
}

// InsertData inserts data into db
func InsertData() {
	// Insert Statement
	database, err := sql.Open("sqlite3", "./pulse.db")
	dbLog.Error(err)
	statement, err := database.Prepare("INSERT INTO registered_apps (app_name, unique_id, unique_secret, created) VALUES (?, ?, ?, ?)")
	dbLog.Error(err)
	res, err := statement.Exec("test_app", "asdfwefasdf", "sadf234rasefasdf", time.Now().Format("2006-01-02 15:04:05"))
	dbLog.Error(err)
	dbLog.Info(res)
	fmt.Println("data inserted")
}

// SelectStatement selects data from database
func SelectStatement() {
	// Select statement
	database, err := sql.Open("sqlite3", "./pulse.db")
	dbLog.Error(err)
	rows, _ := database.Query("SELECT uid, app_name, unique_id, unique_secret, created FROM registered_apps")
	var uid int
	var appName string
	var uniqueID string
	var uniqueSecret string
	var created string
	for rows.Next() {
		err = rows.Scan(&uid, &appName, &uniqueID, &uniqueSecret, &created)
		dbLog.Error(err)
	}
	rows.Close()
	database.Close()
	fmt.Println(uid, appName, uniqueID, uniqueSecret, created)
}
