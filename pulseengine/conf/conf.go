package conf

import (
	"fmt"
	"os"
	"sync"

	"gopkg.in/yaml.v2"
)

// PulseConfig is the global config struct for client
var PulseConfig conf

func init() {
	PulseConfig.ingestConf()
}

// Conf is a struct used to store conf data read in from conf file for client side, server side conf will live withiing sqllite db.
type conf struct {
	Client client
	confMu sync.RWMutex
}

type client struct {
	ClientID      string     `json:"client_id"` // This is provideed by the server post app registration if using pulse server, otherwise this can be any string
	Reporters     []reporter `json:"reporters"`
	StateMonitors []state
}

type reporter struct {
	ReporterName string            `json:"reporter_name"`
	LogPaths     map[string]string `json:"log_paths"`
}

type state struct {
	Name   string `json:"name"`
	Backup bool   `json:"backup"`
	Path   string `json:"path"`
	Alerts bool   `json:"alerts"`
}

// ReadConf Reads in Conf file
func (c *conf) ingestConf() {
	c.confMu.Lock()
	defer c.confMu.Unlock()
	yamlConf, err := os.ReadFile("conf/pulse.conf")
	if err != nil {
		fmt.Println(err)
	}
	err = yaml.Unmarshal(yamlConf, &c)
	if err != nil {
		fmt.Println(err)
	}
}

func (c *conf) ReadConf(name string) interface{} {
	switch name {
	case "client_id":
		return c.Client.ClientID
	case "reporters":
		return c.Client.Reporters
	case "state":
		return c.Client.StateMonitors
	}
	return nil
}
