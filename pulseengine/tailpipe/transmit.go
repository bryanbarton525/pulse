package tailpipe

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type Log struct {
	Title     string
	Author    string
	ISBN      string
	Publisher string
	Copies    int
}

// func transmitPayload() {

// }

func ConnectToDb() {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://root:8F0b0f87C6@10.174.60.12:27017"))
	if err != nil {
		log.Fatal(err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(ctx)
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connected to MongoDB!")

	logsCollection := client.Database("testdb").Collection("logs")

	filter := bson.D{primitive.E{Key: "isbn", Value: "0451526341"}}

	// Insert One document
	log1 := Log{"Animal Farm", "George Orwell", "0451526341", "Signet Classics", 100}
	insertResult, err := logsCollection.InsertOne(context.TODO(), log1)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Inserted a single document: ", insertResult.InsertedID)

	// A variable in which result will be decoded
	var result Log

	err = logsCollection.FindOne(context.TODO(), filter).Decode(&result)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Found a single document: %+v\n", result)

	cursor, err := logsCollection.Find(context.TODO(), bson.D{{}})
	if err != nil {
		log.Fatal(err)
	}
	var logs []Log
	if err = cursor.All(context.TODO(), &logs); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Found multiple documents: %+v\n", logs)
}
