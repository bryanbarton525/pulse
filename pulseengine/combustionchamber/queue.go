package combustionchamber

import (
	"container/list"
	"fmt"

	"gitlab.com/bryanbarton525/pulse/logging"
)

// CreateQueue creates a new queue
func CreateQueue() (queue *list.List) {
	queue = list.New()
	return queue
}

// Enqueue adds item to queue
func Enqueue(message Message, queue *list.List) bool {
	log := logging.Logger("queue.log")
	e := queue.PushBack(message)
	log.Debug("Queue: ", queue)
	log.Debug("Element: ", e)

	return true
}

// ReadFIQueue reads and returns first item in queue.
func ReadFIQueue(queue *list.List) interface{} {
	message := queue.Front().Value
	return message
}

// Dequeue removes first item from queue
func Dequeue(queue *list.List) {
	fm := queue.Front()
	fmt.Println("Removing: ", fm)
	fmt.Println(queue.Len())
	if queue.Len() != 0 {
		queue.Remove(fm)
	}
	fmt.Println("Current Queue:", queue)
}
