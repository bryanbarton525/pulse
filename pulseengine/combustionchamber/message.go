package combustionchamber

import (
	"container/list"
	"encoding/json"
	"fmt"

	"gitlab.com/bryanbarton525/pulse/logging"
)

// Message used to unmarshal json into the same standardized structure
type Message struct {
	ClientID string                 `json:"client_id"`
	Reporter string                 `json:"reporter"`
	Event    map[string]interface{} `json:"event"`
}

// ContentCheck validates that the message contains all the required information to be transmitted
func (m *Message) ContentCheck(message Message) (valid bool) {
	ccLog := logging.Logger("message.log")
	valid = true
	validationMap := make(map[string]interface{})
	md, err := json.Marshal(message)
	if err != nil {
		ccLog.Error(err)
	}
	err = json.Unmarshal(md, &validationMap)
	if err != nil {
		ccLog.Error(err)
	}

	for k, v := range validationMap {
		switch v.(type) {
		case string:
			if v == "" {
				ccLog.Error("Missing value for key: ", k)
				valid = false
			}
		case map[string]interface{}:
			if len(v.(map[string]interface{})) == 0 {
				fmt.Println("Missing value for Key: ", k)
				valid = false
			}
		}
	}
	return valid
}

// BuildMessage assembles the data and preps it for transmit
func (m *Message) BuildMessage(data []byte, message *Message, queue *list.List, ch chan Message) {
	buildMessageLog := logging.Logger("message.log")
	fmt.Println("Building Message", string(data))
	err := json.Unmarshal(data, &message)
	if err != nil {
		buildMessageLog.Error(err)
		close(ch)
	}
	validContent := message.ContentCheck(*message)
	if validContent {
		success := Enqueue(*message, queue)
		if !success {
			buildMessageLog.Error("Unable to queue message")
			close(ch)
		} else {
			ch <- *message
		}
	} else {
		buildMessageLog.Error("Invalid Message Data: ", &message)
		close(ch)
	}
}
