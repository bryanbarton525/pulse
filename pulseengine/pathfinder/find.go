package pathfinder

import (
	"fmt"
	"os"
	"path/filepath"
)

// FindFiles scans directory for all log files inside it and builds a all inclusive config yaml from it
func FindFiles() { // TODO Look into using a tree to dynamically build the file paths and allow for faster searching, break the tree to be individual folders in the the full path
	var files []string
	logRoot := "/var/log"
	err := filepath.Walk(logRoot, func(path string, info os.FileInfo, err error) error {
		files = append(files, path)
		return nil
	})
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(files)
}

// FindFile verifies a file exists in a certain directory
func FindFile(file, path string) bool {
	matches, err := filepath.Glob(path + file)
	if err != nil {
		fmt.Println(err)
		return false
	} else if len(matches) != 0 {
		fmt.Println("found: ", matches)
		return true
	}
	return false
}
