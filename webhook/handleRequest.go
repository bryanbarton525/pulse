package webhook

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/bryanbarton525/pulse/auth"
	"gitlab.com/bryanbarton525/pulse/logging"
	"gitlab.com/bryanbarton525/pulse/metrics"
	"gitlab.com/bryanbarton525/pulse/pulseengine/combustionchamber"
)

var q = combustionchamber.CreateQueue()
var hmLog = logging.Logger("message.log")
var rqLog = logging.Logger("requests.log")

// HandleRequests serves the api and handles routing
func HandleRequests() {
	http.DefaultClient.Timeout = time.Second * 5
	requestLogger := logging.Logger("requests.log")
	myRouter := mux.NewRouter().StrictSlash(true)
	api := myRouter.PathPrefix("/api/v1").Subrouter()
	api.HandleFunc("/auth", auth.CreateToken).Methods("POST")
	api.HandleFunc("/log_event", auth.ValidateMiddleware(handleMessage)).Methods("POST")
	api.Handle("/metrics", promhttp.Handler())
	requestLogger.Fatal(http.ListenAndServe("localhost:20967", myRouter))
}

// handleMessage consumes, validates, processes and transmits data
func handleMessage(w http.ResponseWriter, r *http.Request) {
	message := new(combustionchamber.Message)

	msg := make(map[string]interface{})
	msg["header"] = r.Header
	msg["uri"] = r.RequestURI
	msg["remoteHost"] = r.RemoteAddr

	rqLog.Info(msg)
	req, err := io.ReadAll(r.Body)
	metrics.CurrentOpenFiles.Add(1)
	err = r.Body.Close()
	if err != nil {
		json.NewEncoder(w).Encode("error: an internal error has occured - Unable to close request body")
		return
	}
	metrics.CurrentOpenFiles.Sub(1)
	if err != nil {
		hmLog.Error(err)
		json.NewEncoder(w).Encode("error: unable to parse body")
		return
	}
	ch := make(chan combustionchamber.Message)
	go message.BuildMessage(req, message, q, ch)
	_, ok := <-ch
	if ok == false {
		hmLog.Error("An Error Occured: Channel closed by reciever")
		json.NewEncoder(w).Encode("error: unable to process message")
		return
	}

	json.NewEncoder(w).Encode("success: message processed")
	for q.Len() > 0 {
		queuedMessage := combustionchamber.ReadFIQueue(q)
		// transmit queued message
		fmt.Println(queuedMessage) // Placeholder
		combustionchamber.Dequeue(q)
	}
	metrics.SuccessEventPostCount.Inc()
}
