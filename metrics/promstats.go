package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

// SuccessEventPostCount is a variable for prometheus metric for total number of successful posts of events
var SuccessEventPostCount = promauto.NewCounter(prometheus.CounterOpts{
	Name: "event_post_counter_total",
	Help: "Total number of Events Posted to API",
})

// TotalAuthFailureCount is a variable for prometheus metrics for total token auth failures
var TotalAuthFailureCount = promauto.NewCounter(prometheus.CounterOpts{
	Name: "token_auth_failure_count",
	Help: "Total number of token authentication failures.",
})

// TokenRequestCount is a variable for prometheus metrics for total token auth failures
var TokenRequestCount = promauto.NewCounter(prometheus.CounterOpts{
	Name: "token_auth_request_count",
	Help: "Total number of token authentication requests.",
})

// TokenIssuedCount is a variable for prometheus metrics for total token auth failures
var TokenIssuedCount = promauto.NewCounter(prometheus.CounterOpts{
	Name: "token_issued_count",
	Help: "Total number of tokens issued.",
})
// CurrentOpenFiles is a variable for prometheus metrics for total token auth failures
var CurrentOpenFiles = promauto.NewGauge(prometheus.GaugeOpts{
	Name: "open_file_count",
	Help: "Total number of open response body reads.",
})
