package main

import (
	"gitlab.com/bryanbarton525/pulse/auth"
	"gitlab.com/bryanbarton525/pulse/logging"
	"gitlab.com/bryanbarton525/pulse/pulseengine/tailpipe"
	"gitlab.com/bryanbarton525/pulse/webhook"
)

/*
// Parse yaml file to setup application with correct log files as well as target custom log data
func configParse() {
	// Do things here
}
*/

func main() {
	log := logging.Logger("streamer.log")
	tailpipe.ConnectToDb()
	log.Info("Streaming API Started")
	auth.CreateDB()
	auth.InsertData()
	auth.SelectStatement()
	webhook.HandleRequests()
}
