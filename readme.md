## Lets talk about configs:
### Example Config:
```
pulse_id: client0
reporters:
  - name: network_reporter
    log_path: 
      test_app: /var/log/test_app.log
      test2: test3
```
#### pulse_id: Unique host identifier, this can be anything that helps you identify the device. This value will be used as the primary identifing key when transmitting logs json blobs.

Each registered app should have a unique id and key generated when it is registered with stream. You must declare this info in the config to 

## Things to do:
- Finish example config file
- Create config parser
- Use parsed config to define log files to scrape
- Create placeholder file to denote last read in, this will ensure always grabbing correct logs
- Fix logging to make it easier to follow whats going on
- Add security (jwt validation most likely) for cloud deployments
- Add app registration option. Best option will probably be random token generation and app_id gernation with storage in sqllite DB on host itself. Probably command line but might go with API call(maybe both). This will ensure that rouge apps dont start leveraging pulse-engine to transmit bad data to data stores.
- Add unregistered app reporting both to on host logs and upstream data store to ensure proper escalation happens.
- Add log flooding detection to prevent pipeline saturation and/ or allow for alerts to be generated for such events
- add more prometheus/ grafana metrics
- add more cool shit
